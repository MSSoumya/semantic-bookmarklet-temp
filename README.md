Bookmarklet URL:
https://gl.githack.com/MSSoumya/semantic-bookmarklet-temp/-/raw/master/src/annolet-bookmarklet/annolet/annolet.html

# RenarrationUI - Bookmarklet for Renarration

## Setup server for running webservices
- For lang translation, phonetics translation, currency translation,
measurement conversion on a web page, you require to run
webservice server.

### Steps
1. Clone the following repository:
```
git clone https://gitlab.com/MSSoumya/semantic-bookmarklet-temp.git
```

2. Checkout branch
```
git checkout <branch>
```

3. Install the dependencies
```
make all
```

4. Install the dependencies
```
make install
```

5. Run the server
```
make run
```

## Get RenarrationUI bookmarklet
- Use *Firefox* to run the bookmarklet.

### Steps:
1. Go to https://gl.githack.com/MSSoumya/semantic-bookmarklet-temp/-/raw/master/src/annolet-bookmarklet/annolet/annolet.html

2. Drag the  `Annotate` button onto the browser bookmark bar.

3. Open any webpage of your choice.
   For Example: http://tg.meeseva.gov.in/DeptPortal/UserInterface/LoginForm.aspx

4. Click on a bookmarklet

5. Now you can see the webpage loaded with an annolet

6. Select the content on a webpage and start renarrating...!

## Need to update the sources in annolet-bookmarklet directory when you make changes in org  
### Steps
1. Clone the following repository:
```
git clone https://gitlab.com/MSSoumya/semantic-bookmarklet-temp.git
```

2. Checkout branch
```
git checkout <branch>
```

3. Run Make
```
make 
```

4. update sources
```
make update 
```
