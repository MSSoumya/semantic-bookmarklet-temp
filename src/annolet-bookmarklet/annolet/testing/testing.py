from py2neo import Graph
graph = Graph()
cypher = graph.cypher
cypher.execute("CREATE(n1:NPTEL:Lecture:DataStructures:Algorithms {name:'Computer Science and Engineering - NOC: Programming, Data Structures and Algorithms using Python' , link:'https://nptel.ac.in/courses/106/106/106106145/' })")
results = graph.cypher.execute("MATCH (n:Algorithms) RETURN n")
print(results)
