
var json_data;

getJsondata();

function getJsondata()
{
   disableAllLinks() // disables all the links in a page
   var url = "https://gl.githack.com/MSSoumya/semantic-bookmarklet-temp/-/raw/master/src/annolet-bookmarklet/annolet/config.json";
   $.getJSON(url, function() {
       console.log("Request to config file success..!");
   })
   .done(function(data, jqxhr, textStatus) {
       json_data = data;
       console.log("Retrieved the config data successfully..!");
       annoletContainer();
       addClickevents();
    })
   .fail(function(jqxhr, textStatus, error) {
       var err = textStatus + ", " + error;
       console.log("Request to config file failed: " + ", " + err);
    })
   .always(function() {
       console.log("Request to config_file complete");
    });
}

function annoletContainer(){
   //appending a div(annolet container) to body element of a webpage.
   var body = document.getElementsByTagName("body")[0];
   container = document.createElement("div");
   container.id = "annolet-container";
   container.className = "anno";
   body.appendChild(container);

   //appending a CSS stylesheet into head element of a webpage, which is used to stylize the annolet container.
   var linktag = document.createElement("link");
   linktag.rel = "stylesheet";
   linktag.type = "text/css";
   linktag.href = json_data.css["annolet"];
   document.getElementsByTagName('head')[0].appendChild(linktag);

   //injecting html code
   container.innerHTML = "<h4 id='annolet-header' class='anno'>SeRena</h4>"+"<br>"+"<br>"+
   "<div>"+"<br>"+"<ul id='annolet-tools-menu' class='anno'>"+
       "<li id='disable_list' class='annolet-menu-item anno'>"+
           "<button id='disable-css' class='annolet-menu-sub-item anno' title='Click on the button to view the page without css'>Show Links</button>"+
       "</li>"+"<br>"+"<br>"+
       "<li id='activezapper_list' class='annolet-menu-item anno'  title='Click on the button first and then point out the elements to remove the clutter'>"+
           "<button id='activate-zapper' class='annolet-menu-sub-item anno' >Show Videos</button>"+
       "</li>"+"<br>"+"<br>"+
       "<li id='deactivezapper_list' class='annolet-menu-item anno'>"+
           "<button id='deactivate-zapper' class='annolet-menu-sub-item anno' title='Click on the button to deactivate the zapper'>Show Links</button>"+
       "</li>"+"<br>"+
       "<li id='modify_list' class='annolet-menu-item anno'  title='Click on the button first and then modify the content on a page' >"+
           "<button id='modify-content' class='annolet-menu-sub-item anno' >Modify Content</button>"+
       "</li>"+"<br>"+"<br>"+
   "</ul>"+"</div>";

    setTimeout(function() {
        alert("Page is ready to Renarrate");
    }, 1000);
}

function disableAllLinks(){
   var anchors = document.getElementsByTagName("a");
   for( var i = 0, max = anchors.length; i < max; i++ ) {
       anchors[i].onclick = function() {return(false);};
   }
}

function disableCss(){
   // loops through all the external stylesheets and disables it
   var style_sheets = document.styleSheets;
   for( var i = 0, max = style_sheets.length; i < max; i++ ) {
      if(style_sheets[i].href === json_data.css["annolet"] ) {
         style_sheets[i].disabled = false;
      }
      else {
         style_sheets[i].disabled = true;
      }
   }

   // loops through all the body elems and disables the inline css
   var body_elems = document.body.getElementsByTagName("*");
   for( var i = 0, max = body_elems.length; i < max; i++ ) {
      var style_attr = $(body_elems[i]).attr("style");
      if(style_attr) {
         $(body_elems[i].tagName).removeAttr("style");
      }
   }

   // loops through all the head elems and disables the internal css
   var head_elems = document.head.getElementsByTagName("*");
   for( var i = 0, max = head_elems.length; i < max; i++ ) {
       if(head_elems[i].tagName === "STYLE") {
          $(head_elems[i].tagName).remove();
       }
   }
}

function ZapPer(){
   $("body").click(function(event){
      var target_elem = event.target;
      if( target_elem.classList.contains("anno") === True ){
          target_elem.style.visibility = "visible";
      }
      else {
          target_elem.style.visibility = "hidden";
      }
   });
}

function deactivateZapPer() {
   alert("Zapper deactivated");
   $('body').unbind("click");
   $('activate-zapper').unbind("click");
}

function modifyContent() {
   var all = document.getElementsByTagName("*");
   for(var i = 0, max = all.length; i < max; i++) {
      if(all[i].classList.contains("anno") === false) {
         all[i].setAttribute("contenteditable", true);
         all[i].setAttribute("title", "Modify Content");
      }
   }
}

function annoHighlight() {
   var current = getSelectedText();
   if(current !== "") {
      // highlightContent(current);
      highlightContent("TEMPORARY CONTENT");
   }
   else if(current === "") {
      alert("Select text and click on highlighter button");
   }
}

// function getSelectedText(){
//    if(window.getSelection){
//       return window.getSelection().toString();
//    }
//    else if(document.getSelection){
//       return document.getSelection();
//    }
//    else if(document.selection){
//       return document.selection.createRange().text;
//    }
//    return '';
// }

function getSelectedText() {
                var selectedText = '';

                // window.getSelection
                if (window.getSelection) {
                    selectedText = window.getSelection();
                }
                // document.getSelection
                else if (document.getSelection) {
                    selectedText = document.getSelection();
                }
                // document.selection
                else if (document.selection) {
                    selectedText =
                    document.selection.createRange().text;
                } else return;
                // To write the selected text into the textarea
                document.testform.selectedtext.value = selectedText;
            }

function highlightContent(sel_text) {
   var span = document.createElement("span");
   span.textContent = sel_text;
   span.style.backgroundColor = "yellow";
   span.id = "highlighted";
   span.className = "anno";
   var range = sel_text.getRangeAt(0);
   range.deleteContents();
   range.insertNode(span);
}

function addClickevents(){
   document.getElementById('disable-css').addEventListener('click', function() {
      disableCss()
   }, false);
   document.getElementById('activate-zapper').addEventListener('click', function() {
      ZapPer()
   }, false);
   document.getElementById('deactivate-zapper').addEventListener('click', function() {
      deactivateZapPer()
   }, false);
   document.getElementById('modify-content').addEventListener('click', function() {
      modifyContent()
   }, false);
   document.getElementById('change-font').addEventListener('click', function() {
      changeFont()
   }, false);
}
